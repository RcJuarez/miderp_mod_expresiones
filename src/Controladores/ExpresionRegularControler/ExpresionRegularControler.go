package ExpresionRegularControler

import (
	"encoding/json"
	"fmt"
	"html/template"
	"regexp"
	"strconv"
	"time"

	"../../Modelos/ExpresionRegularModel"
	"../../Modulos/ExpresionesRedis"
	"../../Modulos/General"
	"gopkg.in/kataras/iris.v6"
	"gopkg.in/mgo.v2/bson"
)

//##########< Variables Generales > ############

var cadenaBusqueda string
var numeroRegistros int
var paginasTotales int

//NumPagina especifica el numero de página en la que se cargarán los registros
var NumPagina int

//limitePorPagina limite de registros a mostrar en la pagina
var limitePorPagina = 10

//IDElastic id obtenido de Elastic
var IDElastic bson.ObjectId
var arrIDMgo []bson.ObjectId
var arrIDElastic []bson.ObjectId
var arrToMongo []bson.ObjectId

//####################< INDEX (BUSQUEDA) >###########################

//IndexGet renderea al index de Regexp
func IndexGet(ctx *iris.Context) {

	var Send ExpresionRegularModel.SExpresionRegular

	// NameUsrLoged, MenuPrincipal, MenuUsr, errSes := Session.GetDataSession(ctx)
	// Send.SSesion.Name = NameUsrLoged
	// Send.SSesion.MenuPrincipal = template.HTML(MenuPrincipal)
	// Send.SSesion.MenuUsr = template.HTML(MenuUsr)
	// if errSes != nil {
	// 	Send.SEstado = false
	// 	Send.SMsj = errSes.Error()
	// 	ctx.Render("ZError.html", Send)
	// 	return
	// }

	ctx.Render("RegexpIndex.html", Send)

}

//CargaIndex regresa todos los datos necesarios para cargar el index de Regexp
func CargaIndex(ctx *iris.Context) {

	var Send ExpresionRegularModel.SExpresionRegular

	// NameUsrLoged, MenuPrincipal, MenuUsr, errSes := Session.GetDataSession(ctx)
	// Send.SSesion.Name = NameUsrLoged
	// Send.SSesion.MenuPrincipal = template.HTML(MenuPrincipal)
	// Send.SSesion.MenuUsr = template.HTML(MenuUsr)
	// if errSes != nil {
	// 	Send.SEstado = false
	// 	Send.SMsj = errSes.Error()
	// 	ctx.Render("ZError.html", Send)
	// 	return
	// }
	fmt.Println("Entro a cargar index")
	Send.SIndex.STituloTabla = "Se han cargado todos(as) los(as) Regexps"
	Modelo := ExpresionRegularModel.MapaModelo()
	Send.SIndex.SNombresDeColumnas = Modelo["name"].([]string)
	Send.SIndex.SModeloDeColumnas = MoGeneral.GeneraModeloColumnas(Modelo)
	Send.SIndex.SRenglones = ExpresionRegularModel.GeneraRenglonesIndex(ExpresionRegularModel.GetAll())
	Send.SEstado = true

	jData, err := json.Marshal(Send)
	if err != nil {
		fmt.Println(err)
	}
	ctx.Header().Set("Content-Type", "application/json")
	ctx.Write(jData)
	return

}

//CargaIndexEspecifico regresa los datos filtrados necesarios para cargar el index de Regexp
func CargaIndexEspecifico(ctx *iris.Context) {

	var Send ExpresionRegularModel.SExpresionRegular

	// NameUsrLoged, MenuPrincipal, MenuUsr, errSes := Session.GetDataSession(ctx)
	// Send.SSesion.Name = NameUsrLoged
	// Send.SSesion.MenuPrincipal = template.HTML(MenuPrincipal)
	// Send.SSesion.MenuUsr = template.HTML(MenuUsr)
	// if errSes != nil {
	// 	Send.SEstado = false
	// 	Send.SMsj = errSes.Error()
	// 	ctx.Render("ZError.html", Send)
	// 	return
	// }
	fmt.Println("Entro a expresion regular controles index nuevo ")
	var TextoABuscar string
	TextoABuscar = ctx.FormValue("Texto")
	if TextoABuscar != "" {

		Send.SIndex.STituloTabla = "Se cargaron los(as) Regexps filtrados(as) por: " + TextoABuscar
		Modelo := ExpresionRegularModel.MapaModelo()
		Send.SIndex.SNombresDeColumnas = Modelo["name"].([]string)
		Send.SIndex.SModeloDeColumnas = MoGeneral.GeneraModeloColumnas(Modelo)
		docs, err := ExpresionRegularModel.BuscarEnElastic(TextoABuscar)
		if err != nil {
			fmt.Println(err)
			Send.SEstado = false
			Send.SMsj += " Ocurrió un error al consultar : " + err.Error()
			jData, err := json.Marshal(Send)

			if err != nil {
				fmt.Println(err)
			}
			ctx.Header().Set("Content-Type", "application/json")
			ctx.Write(jData)
			return
		}

		if docs.Hits.TotalHits > 0 {

			arrIDElastic = []bson.ObjectId{}
			for _, item := range docs.Hits.Hits {
				IDElastic = bson.ObjectIdHex(item.Id)
				arrIDElastic = append(arrIDElastic, IDElastic)
			}

		} else {

			Send.SEstado = false
			Send.SMsj += "No se encontraron resultados para: " + TextoABuscar

			jData, err := json.Marshal(Send)
			if err != nil {
				fmt.Println(err)
			}

			ctx.Header().Set("Content-Type", "application/json")
			ctx.Write(jData)
			return
		}

		fmt.Println("Se encontraron ", len(arrIDElastic), " registros.")
		Send.SIndex.SRenglones = ExpresionRegularModel.GeneraRenglonesIndex(ExpresionRegularModel.GetEspecifics(arrIDElastic))
		fmt.Println(Send.SIndex.SRenglones)
		Send.SEstado = true
		jData, err := json.Marshal(Send)
		if err != nil {
			fmt.Println(err)
		}
		fmt.Println(jData)
		ctx.Header().Set("Content-Type", "application/json")

		ctx.Write(jData)

		return
	}

	jData, err := json.Marshal(Send)
	if err != nil {
		fmt.Println(err)
	}

	Send.SEstado = false
	Send.SMsj += " Se recibió una cadena de texto vacía, ingrese un texto a buscar e intente de nuevo."
	ctx.Header().Set("Content-Type", "application/json")

	ctx.Write(jData)

	return

}

//###########################< ALTA >################################

//AltaGet renderea al alta de ExpresionRegular
func AltaGet(ctx *iris.Context) {

	var Send ExpresionRegularModel.SExpresionRegular

	/*NameUsrLoged, MenuPrincipal, MenuUsr, errSes := Session.GetDataSession(ctx) //Retorna los datos de la session
	Send.SSesion.Name = NameUsrLoged
	Send.SSesion.MenuPrincipal = template.HTML(MenuPrincipal)
	Send.SSesion.MenuUsr = template.HTML(MenuUsr)
	if errSes != nil {
		Send.SEstado = false
		Send.SMsj = errSes.Error()
		ctx.Render("ZError.html", Send)
		return
	}*/

	//####   TÚ CÓDIGO PARA CARGAR DATOS A LA VISTA DE ALTA----> PROGRAMADOR

	ctx.Render("ExpresionRegularAlta.html", Send)

}

//AltaPost regresa la petición post que se hizo desde el alta de ExpresionRegular
func AltaPost(ctx *iris.Context) {

	var Send ExpresionRegularModel.SExpresionRegular

	EstatusPeticion := false

	/*NameUsrLoged, MenuPrincipal, MenuUsr, errSes := Session.GetDataSession(ctx) //Retorna los datos de la session
	Send.SSesion.Name = NameUsrLoged
	Send.SSesion.MenuPrincipal = template.HTML(MenuPrincipal)
	Send.SSesion.MenuUsr = template.HTML(MenuUsr)
	if errSes != nil {
		Send.SEstado = false
		Send.SMsj = errSes.Error()
		ctx.Render("ZError.html", Send)
		return
	}*/
	//####   TÚ CÓDIGO PARA PROCESAR DATOS DE LA VISTA DE ALTA Y GUARDARLOS O REGRESARLOS----> PROGRAMADOR

	var exp ExpresionRegularModel.ExpresionRegularMgo //Variable estructura para recibir los datos.

	fmt.Println(ctx.FormValues())

	exp.FechaHora = time.Now()
	exp.ID = bson.NewObjectId()

	Nombre := ctx.FormValue("Nombre")
	Send.ExpresionRegular.ENombreExpresionRegular.Nombre = Nombre
	exp.Nombre = Nombre

	if Nombre == "" {
		EstatusPeticion = true
		Send.ExpresionRegular.ENombreExpresionRegular.IEstatus = true
		Send.ExpresionRegular.ENombreExpresionRegular.IMsj = "Campo Nombre es obligatorio"

	}

	Expresion := ctx.FormValue("Expresion")
	Send.ExpresionRegular.EExpresionExpresionRegular.Expresion = Expresion
	exp.Expresion = Expresion

	if Expresion == "" {
		EstatusPeticion = true
		Send.ExpresionRegular.EExpresionExpresionRegular.IEstatus = true
		Send.ExpresionRegular.EExpresionExpresionRegular.IMsj = "Campo Expresión es obligatorio"

	}

	exp.Estatus = bson.NewObjectId()

	etiquetas := ctx.Request.Form["Etiquetas"]

	if len(etiquetas) > 0 {
		Send.ExpresionRegular.EEtiquetasExpresionRegular.Ihtml = template.HTML(creaHTMLEtiquetas(etiquetas))
		exp.Etiquetas = etiquetas
	} else {
		EstatusPeticion = true
		Send.ExpresionRegular.EEtiquetasExpresionRegular.IEstatus = true
		Send.ExpresionRegular.EEtiquetasExpresionRegular.IMsj = "Debe contener al menos una etiqueta"
	}

	fmt.Println(exp)

	if EstatusPeticion {
		Send.SEstado = false                                                           //En la vista los errores se manejan al reves para hacer uso del rellenado por defecto de Go
		Send.SMsj = "La validación indica que el objeto capturado no puede procesarse" //La idea es después hacer un colector de errores y mensaje de éxito y enviarlo en esta variable.
		ctx.Render("ExpresionRegularAlta.html", Send)
	} else {

		if exp.InsertaMgo() {
			//Inserta en elastic
			if exp.InsertaElastic() {
				//Inserta en redis
				if ExprRedis.AltaExpresion(exp.ID.Hex(), Nombre, Expresion, etiquetas, time.Now().String()) {
					Send.SEstado = true
					Send.SMsj = "Se ha realizado una inserción exitosa"
					//SE PUEDE TOMA LA DECICIÓN QUE SE CREA MÁS PERTINENTE, EN ESTE CASO SE CONSIDERA EL DETALLE DEL OBJETO.
					ctx.Redirect("/ExpresionRegulars/detalle/"+exp.ID.Hex(), 301)
				} else {
					Send.SEstado = false
					Send.SMsj = "Ocurrió un error al insertar en redis"
					ctx.Render("ExpresionRegularAlta.html", Send)
				}
			} else {
				Send.SEstado = false
				Send.SMsj = "Ocurrió un error al insertar en elasticSearch"
				ctx.Render("ExpresionRegularAlta.html", Send)
			}

		} else {
			Send.SEstado = false
			Send.SMsj = "Ocurrió un error al insertar en MongoDb"
			ctx.Render("ExpresionRegularAlta.html", Send)
		}

	}

}

//###########################< EDICION >###############################

//EditaGet renderea a la edición de ExpresionRegular
func EditaGet(ctx *iris.Context) {
	var Send ExpresionRegularModel.SExpresionRegular

	/*NameUsrLoged, MenuPrincipal, MenuUsr, errSes := Session.GetDataSession(ctx) //Retorna los datos de la session
	Send.SSesion.Name = NameUsrLoged
	Send.SSesion.MenuPrincipal = template.HTML(MenuPrincipal)
	Send.SSesion.MenuUsr = template.HTML(MenuUsr)
	if errSes != nil {
		Send.SEstado = false
		Send.SMsj = errSes.Error()
		ctx.Render("ZError.html", Send)
		return
	}*/
	id := ctx.Param("ID") //obtener parametros por get
	Expresion := ExpresionRegularModel.GetOne(bson.ObjectIdHex(id))
	Send.ExpresionRegular.ID = bson.ObjectIdHex(id)
	Send.ExpresionRegular.ENombreExpresionRegular.Nombre = Expresion.Nombre
	Send.ExpresionRegular.EExpresionExpresionRegular.Expresion = Expresion.Expresion
	Send.ExpresionRegular.EEtiquetasExpresionRegular.Ihtml = template.HTML(creaHTMLEtiquetas(Expresion.Etiquetas))

	cadena := Expresion.Expresion
	compara := ctx.FormValue("Evaluar")
	respuesta := Evaluarcadena(cadena, compara)
	fmt.Println(respuesta)

	ctx.Render("ExpresionRegularEdita.html", Send)
}

//EditaPost regresa el resultado de la petición post generada desde la edición de ExpresionRegular
func EditaPost(ctx *iris.Context) {

	var Send ExpresionRegularModel.SExpresionRegular

	/*NameUsrLoged, MenuPrincipal, MenuUsr, errSes := Session.GetDataSession(ctx) //Retorna los datos de la session
	Send.SSesion.Name = NameUsrLoged
	Send.SSesion.MenuPrincipal = template.HTML(MenuPrincipal)
	Send.SSesion.MenuUsr = template.HTML(MenuUsr)
	if errSes != nil {
		Send.SEstado = false
		Send.SMsj = errSes.Error()
		ctx.Render("ZError.html", Send)
		return
	}*/

	id := ctx.Param("ID") //obtener parametros por get
	convierte := bson.ObjectIdHex(id)
	Expresion := ExpresionRegularModel.GetOne(bson.ObjectIdHex(id))
	Send.ExpresionRegular.ID = bson.ObjectIdHex(id)
	Send.ExpresionRegular.ENombreExpresionRegular.Nombre = Expresion.Nombre
	Send.ExpresionRegular.EExpresionExpresionRegular.Expresion = Expresion.Expresion
	Send.ExpresionRegular.EEtiquetasExpresionRegular.Etiquetas = Expresion.Etiquetas
	fmt.Println(id)
	fmt.Println(Expresion)

	var Vexp ExpresionRegularModel.ExpresionRegular
	var Mexp ExpresionRegularModel.ExpresionRegularMgo
	//Estructura para guardar en redis
	var Rexp ExpresionRegularModel.ExpresionRegularRedis

	NNombre := ctx.FormValue("Nombre")
	NExpresion := ctx.FormValue("Expresion")
	NEtiqueta := ctx.Request.Form["Etiquetas"]
	Vexp.ENombreExpresionRegular.Nombre = NNombre
	Vexp.EExpresionExpresionRegular.Expresion = NExpresion
	Vexp.EEtiquetasExpresionRegular.Etiquetas = NEtiqueta //Valores devista

	Mexp.ID = convierte
	Rexp.ID = convierte.Hex()
	Mexp.Nombre = NNombre
	Rexp.Nombre = NNombre
	Mexp.Expresion = NExpresion
	Rexp.Expresion = NExpresion
	Mexp.Etiquetas = NEtiqueta
	Rexp.Etiquetas = NEtiqueta

	if NNombre == "" { //campo delnombre.
		Vexp.ENombreExpresionRegular.IEstatus = true
		Vexp.ENombreExpresionRegular.IMsj = "Se requiere un nombre de Expresion"
	} else if len(NNombre) < 5 || len(NNombre) > 50 {
		Vexp.ENombreExpresionRegular.IEstatus = true
		Vexp.ENombreExpresionRegular.IMsj = "Elnombre deber ser  mayor a 5 y menor de 50"
	}
	if NExpresion == "" { //campo de la Expresion

		Vexp.EExpresionExpresionRegular.IEstatus = true
		Vexp.EExpresionExpresionRegular.IMsj = "Se requiere una Expresion"
	} else if len(NExpresion) < 1 || len(NExpresion) > 250 {

		Vexp.EExpresionExpresionRegular.IEstatus = true
		Vexp.EExpresionExpresionRegular.IMsj = "La expresion regular debe ser mayor a 1 y menor a 250"
	}

	if NEtiqueta == nil {
		Vexp.EExpresionExpresionRegular.IEstatus = true
		Vexp.EExpresionExpresionRegular.IMsj = "Se requiere al menos una etiqueta"
	}
	//Mandar valores a mongo.
	resp := Mexp.ActualizaMgo([]string{"Nombre", "Expresion", "Etiquetas"}, []interface{}{Mexp.Nombre, Mexp.Expresion, Mexp.Etiquetas})
	fmt.Println(resp)
	fmt.Println(NNombre)
	fmt.Println(NExpresion)
	fmt.Println(NEtiqueta)

	Mexp.ActualizaElastic()

	ExprRedis.ActualizaExpresion(Rexp)

	//####   TÚ CÓDIGO PARA PROCESAR DATOS DE LA VISTA DE ALTA Y GUARDARLOS O REGRESARLOS----> PROGRAMADOR

	ctx.Render("ExpresionRegularEdita.html", Send)

}

//#################< DETALLE >####################################

//DetalleGet renderea al index.html
func DetalleGet(ctx *iris.Context) {
	var Send ExpresionRegularModel.SExpresionRegular
	/*NameUsrLoged, MenuPrincipal, MenuUsr, errSes := Session.GetDataSession(ctx) //Retorna los datos de la session
	Send.SSesion.Name = NameUsrLoged
	Send.SSesion.MenuPrincipal = template.HTML(MenuPrincipal)
	Send.SSesion.MenuUsr = template.HTML(MenuUsr)
	if errSes != nil {
		Send.SEstado = false
		Send.SMsj = errSes.Error()
		ctx.Render("ZError.html", Send)
		return
	}*/
	//###### TU CÓDIGO AQUÍ PROGRAMADOR
	id := ctx.Param("ID") //obtener parametros por get
	Expresion := ExpresionRegularModel.GetOne(bson.ObjectIdHex(id))
	Send.ExpresionRegular.ID = bson.ObjectIdHex(id)
	Send.ExpresionRegular.ENombreExpresionRegular.Nombre = Expresion.Nombre
	Send.ExpresionRegular.EExpresionExpresionRegular.Expresion = Expresion.Expresion
	//Send.ExpresionRegular.EEtiquetasExpresionRegular.Etiquetas = Expresion.Etiquetas
	Send.ExpresionRegular.EEtiquetasExpresionRegular.Ihtml = template.HTML(creaHTMLEtiquetasDetalle(Expresion.Etiquetas))

	cadena := Expresion.Expresion
	compara := ctx.FormValue("Evaluar")
	respuesta := Evaluarcadena(cadena, compara)
	fmt.Println(respuesta)

	ctx.Render("ExpresionRegularDetalle.html", Send)

} //cerrar llave
//EvaluarGet prepara la funcion de evaluaget
func EvaluarGet(ctx *iris.Context) {
	var Send ExpresionRegularModel.SExpresionRegular
	id := ctx.FormValue("ID") //obtener parametros por get
	Expresion := ExpresionRegularModel.GetOne(bson.ObjectIdHex(id))
	Send.ExpresionRegular.ENombreExpresionRegular.Nombre = Expresion.Nombre
	Send.ExpresionRegular.EExpresionExpresionRegular.Expresion = Expresion.Expresion
	Send.ExpresionRegular.EEtiquetasExpresionRegular.Etiquetas = Expresion.Etiquetas

	cadena := Expresion.Expresion
	compara := ctx.FormValue("Evaluar")
	respuesta := Evaluarcadena(cadena, compara)
	fmt.Println(cadena)
	fmt.Println(compara)
	fmt.Println(respuesta)

	ctx.Text(200, strconv.FormatBool(respuesta))
}

//Evaluarcadena funcion para evaluar cadena
func Evaluarcadena(Expresion string, cadena string) bool {

	re := regexp.MustCompile("^" + Expresion + "$")
	response := re.MatchString(cadena)
	//fmt.Println(re)
	//match, error := regexp.MatchString(Expresion, cadena)
	return response
}

//DetallePost renderea al index.html
func DetallePost(ctx *iris.Context) {
	var Send ExpresionRegularModel.SExpresionRegular

	/*NameUsrLoged, MenuPrincipal, MenuUsr, errSes := Session.GetDataSession(ctx) //Retorna los datos de la session
	Send.SSesion.Name = NameUsrLoged
	Send.SSesion.MenuPrincipal = template.HTML(MenuPrincipal)
	Send.SSesion.MenuUsr = template.HTML(MenuUsr)
	if errSes != nil {
		Send.SEstado = false
		Send.SMsj = errSes.Error()
		ctx.Render("ZError.html", Send)
		return
	}*/

	//###### TU CÓDIGO AQUÍ PROGRAMADOR

	ctx.Render("ExpresionRegularDetalle.html", Send)
}

//####################< RUTINAS ADICIONALES >##########################

//creaHTMLEtiquetas devuelve los trs dormados de las etiquetas
func creaHTMLEtiquetas(etiquetas []string) string {
	htmle := ``
	for i := 0; i < len(etiquetas); i++ {
		htmle += `<tr>
				<td><input type="hidden" class="form-control" value=""><input type="text" class="form-control etiquetas" name="Etiquetas" value="` + etiquetas[i] + `" readonly></td>
				<td><button type="button" class="btn btn-primary editButton"><span class="glyphicon glyphicon-pencil btn-xs"></span></button><button type="button" class="btn btn-danger deleteButton"><span class="glyphicon glyphicon-trash btn-xs"></span></button></td>
				</tr>`
	}

	return htmle
}

//creaHTMLEtiquetas devuelve los trs dormados de las etiquetas
func creaHTMLEtiquetasDetalle(etiquetas []string) string {
	htmle := ``
	for i := 0; i < len(etiquetas); i++ {
		htmle += `<tr>
				<td><input type="hidden" class="form-control" value=""><input type="text" class="form-control etiquetas" name="Etiquetas" value="` + etiquetas[i] + `" readonly></td>
				</tr>`
	}

	return htmle
}
