package ExpresionRegularModel

import (
	"html/template"
	"time"

	"gopkg.in/mgo.v2/bson"
)

//#########################< ESTRUCTURAS >##############################

//EIdExpresionRegular Estructura de campo de ExpresionRegular
type EIdExpresionRegular struct {
	Id string
	//	ID      bson.ObjectId //*
	IEstatus bool
	IMsj     string
	Ihtml    template.HTML
}

//ENombreExpresionRegular Estructura de campo de ExpresionRegular
type ENombreExpresionRegular struct {
	Nombre   string
	IEstatus bool
	IMsj     string
	Ihtml    template.HTML
}

//Evaluacion estructura para evaluar una expresion regular
type Evaluacion struct {
	Evaluacion string
	Respuesta  bool
}

//EExpresionExpresionRegular Estructura de campo de ExpresionRegular
type EExpresionExpresionRegular struct {
	Expresion string
	IEstatus  bool
	IMsj      string
	Ihtml     template.HTML
}

//EEtiquetasExpresionRegular Estructura de campo de ExpresionRegular
type EEtiquetasExpresionRegular struct {
	//Etiquetas string
	Etiquetas []string
	IEstatus  bool
	IMsj      string
	Ihtml     template.HTML
}

//EEstatusExpresionRegular Estructura de campo de ExpresionRegular
type EEstatusExpresionRegular struct {
	Estatus  bson.ObjectId
	IEstatus bool
	IMsj     string
	Ihtml    template.HTML
}

//EFechaHoraExpresionRegular Estructura de campo de ExpresionRegular
type EFechaHoraExpresionRegular struct {
	FechaHora time.Time
	IEstatus  bool
	IMsj      string
	Ihtml     template.HTML
}

//ExpresionRegular estructura de ExpresionRegulars mongo
type ExpresionRegular struct {
	ID bson.ObjectId
	EIdExpresionRegular
	ENombreExpresionRegular
	EExpresionExpresionRegular
	EEtiquetasExpresionRegular
	EEstatusExpresionRegular
	EFechaHoraExpresionRegular
	Evaluacion
}

//SSesion estructura de variables de sesion de Usuarios del sistema
type SSesion struct {
	Name          string
	MenuPrincipal template.HTML
	MenuUsr       template.HTML
}

//SIndex estructura de variables de index
type SIndex struct {
	SResultados        bool
	SRMsj              string
	STituloTabla       string
	SUrlDeDatos        string
	SNombresDeColumnas []string
	SModeloDeColumnas  []map[string]interface{}
	SRenglones         map[string]interface{}
}

//SExpresionRegular estructura de  para la vista
type SExpresionRegular struct {
	SEstado bool
	SMsj    string
	ExpresionRegular
	SIndex
	SSesion
}
