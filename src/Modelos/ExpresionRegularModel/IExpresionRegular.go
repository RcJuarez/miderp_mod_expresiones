package ExpresionRegularModel

import (
	"fmt"

	"gopkg.in/mgo.v2/bson"

	"../../Modulos/Conexiones"
	"../../Modulos/Variables"
)

//IExpresionRegular interface con los métodos de la clase
type IExpresionRegular interface {
	InsertaMgo() bool
	InsertaElastic() bool

	ActualizaMgo(campos []string, valores []interface{}) bool
	ActualizaElastic(campos []string, valores []interface{}) bool //Reemplaza No Actualiza

	ReemplazaMgo() bool
	ReemplazaElastic() bool

	ConsultaExistenciaByFieldMgo(field string, valor string)

	ConsultaExistenciaByIDMgo() bool
	ConsultaExistenciaByIDElastic() bool

	EliminaByIDMgo() bool
	EliminaByIDElastic() bool
}

//################################################<<METODOS DE GESTION >>################################################################

//##################################<< INSERTAR >>###################################

//InsertaMgo es un método que crea un registro en Mongo
func (p ExpresionRegularMgo) InsertaMgo() bool {
	result := false
	ExpresionRegulars, err := MoConexion.GetColectionMgo(MoVar.ColeccionExpresionRegular)
	if err != nil {
		fmt.Println(err)
	}

	err = ExpresionRegulars.Insert(p)
	if err != nil {
		fmt.Println(err)
	} else {
		result = true
	}

	return result
}

//InsertaElastic es un método que crea un registro en Mongo
func (p ExpresionRegularMgo) InsertaElastic() bool {
	var ExpresionRegularE ExpresionRegularElastic

	ExpresionRegularE.Id = p.ID.Hex()
	ExpresionRegularE.Nombre = p.Nombre
	ExpresionRegularE.Expresion = p.Expresion
	ExpresionRegularE.Etiquetas = p.Etiquetas
	ExpresionRegularE.Estatus = p.Estatus
	ExpresionRegularE.FechaHora = p.FechaHora
	insert := MoConexion.InsertaElastic(MoVar.TipoExpresionRegular, p.ID.Hex(), ExpresionRegularE)
	if !insert {
		fmt.Println("Error al insertar ExpresionRegular en Elastic")
		return false
	}
	return true
}

//##########################<< UPDATE >>############################################

//ActualizaMgo es un método que encuentra y Actualiza un registro en Mongo
//IMPORTANTE --> Debe coincidir el número y orden de campos con el de valores
func (p ExpresionRegularMgo) ActualizaMgo(campos []string, valores []interface{}) bool {
	result := false
	ExpresionRegulars, err := MoConexion.GetColectionMgo(MoVar.ColeccionExpresionRegular)
	var Abson bson.M
	Abson = make(map[string]interface{})
	for k, v := range campos {
		Abson[v] = valores[k]
	}
	change := bson.M{"$set": Abson}
	err = ExpresionRegulars.Update(bson.M{"_id": p.ID}, change)
	if err != nil {
		fmt.Println(err)
	} else {
		result = true
	}

	return result
}

//ActualizaElastic es un método que encuentra y Actualiza un registro en Mongo
func (p ExpresionRegularMgo) ActualizaElastic() bool {
	delete := MoConexion.DeleteElastic(MoVar.TipoExpresionRegular, p.ID.Hex())
	if !delete {
		fmt.Println("Error al actualizar ExpresionRegular en Elastic")
		return false
	}

	if !p.InsertaElastic() {
		fmt.Println("Error al actualizar ExpresionRegular en Elastic, se perdió Referencia.")
		return false
	}

	return true
}

//##########################<< REEMPLAZA >>############################################

//ReemplazaMgo es un método que encuentra y Actualiza un registro en Mongo
func (p ExpresionRegularMgo) ReemplazaMgo() bool {
	result := false
	ExpresionRegulars, err := MoConexion.GetColectionMgo(MoVar.ColeccionExpresionRegular)
	err = ExpresionRegulars.Update(bson.M{"_id": p.ID}, p)
	if err != nil {
		fmt.Println(err)
	} else {
		result = true
	}
	return result
}

//ReemplazaElastic es un método que encuentra y reemplaza un ExpresionRegular en elastic
func (p ExpresionRegularMgo) ReemplazaElastic() bool {
	delete := MoConexion.DeleteElastic(MoVar.TipoExpresionRegular, p.ID.Hex())
	if !delete {
		fmt.Println("Error al actualizar ExpresionRegular en Elastic")
		return false
	}
	insert := MoConexion.InsertaElastic(MoVar.TipoExpresionRegular, p.ID.Hex(), p)
	if !insert {
		fmt.Println("Error al actualizar ExpresionRegular en Elastic")
		return false
	}
	return true
}

//###########################<< CONSULTA EXISTENCIAS >>###################################

//ConsultaExistenciaByFieldMgo es un método que verifica si un registro existe en Mongo indicando un campo y un valor string
func (p ExpresionRegularMgo) ConsultaExistenciaByFieldMgo(field string, valor string) bool {
	result := false
	ExpresionRegulars, err := MoConexion.GetColectionMgo(MoVar.ColeccionExpresionRegular)
	if err != nil {
		fmt.Println(err)
	}
	n, e := ExpresionRegulars.Find(bson.M{field: valor}).Count()
	if e != nil {
		fmt.Println(e)
	}
	if n > 0 {
		result = true
	}

	return result
}

//ConsultaExistenciaByIDMgo es un método que encuentra un registro en Mongo buscándolo por ID
func (p ExpresionRegularMgo) ConsultaExistenciaByIDMgo() bool {
	result := false
	ExpresionRegulars, err := MoConexion.GetColectionMgo(MoVar.ColeccionExpresionRegular)
	if err != nil {
		fmt.Println(err)
	}
	n, e := ExpresionRegulars.Find(bson.M{"_id": p.ID}).Count()
	if e != nil {
		fmt.Println(e)
	}
	if n > 0 {
		result = true
	}

	return result
}

//ConsultaExistenciaByIDElastic es un método que encuentra un registro en Mongo buscándolo por ID
func (p ExpresionRegularMgo) ConsultaExistenciaByIDElastic() bool {
	result := MoConexion.ConsultaElastic(MoVar.TipoExpresionRegular, p.ID.Hex())
	return result
}

//##################################<< ELIMINACIONES >>#################################################

//EliminaByIDMgo es un método que elimina un registro en Mongo
func (p ExpresionRegularMgo) EliminaByIDMgo() bool {
	result := false
	ExpresionRegulars, err := MoConexion.GetColectionMgo(MoVar.ColeccionExpresionRegular)
	if err != nil {
		fmt.Println(err)
	}
	e := ExpresionRegulars.RemoveId(bson.M{"_id": p.ID})
	if e != nil {
		result = true
	} else {
		fmt.Println(e)
	}

	return result
}

//EliminaByIDElastic es un método que elimina un registro en Mongo
func (p ExpresionRegularMgo) EliminaByIDElastic() bool {
	delete := MoConexion.DeleteElastic(MoVar.TipoExpresionRegular, p.ID.Hex())
	if !delete {
		fmt.Println("Error al actualizar ExpresionRegular en Elastic")
		return false
	}
	return true
}
