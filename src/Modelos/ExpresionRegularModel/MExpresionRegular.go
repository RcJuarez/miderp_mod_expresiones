package ExpresionRegularModel

import (
	"fmt"
	"strconv"
	"time"

	"../../Modulos/Conexiones"
	"../../Modulos/General"

	"../../Modulos/Variables"
	"gopkg.in/mgo.v2/bson"
	"gopkg.in/olivere/elastic.v5"
)

//#########################< ESTRUCTURAS >##############################

//ExpresionRegularMgo estructura de ExpresionRegulars mongo
type ExpresionRegularMgo struct {
	ID        bson.ObjectId `bson:"_id,omitempty"`
	Nombre    string        `bson:"Nombre"`
	Expresion string        `bson:"Expresion"`
	Etiquetas []string      `bson:"Etiquetas"`
	Estatus   bson.ObjectId `bson:"Estatus"`
	FechaHora time.Time     `bson:"FechaHora"`
}

//ExpresionRegularElastic estructura de ExpresionRegulars para insertar en Elastic
type ExpresionRegularElastic struct {
	Id        string        `json:"ID"`
	Nombre    string        `json:"Nombre"`
	Expresion string        `json:"Expresion"`
	Etiquetas []string      `json:"Etiquetas"`
	Estatus   bson.ObjectId `json:"Estatus"`
	FechaHora time.Time     `json:"FechaHora"`
}

//RegularExpresionRedis estructura de ExpresionRegulars para insertar en redis
type ExpresionRegularRedis struct {
	ID        string   `json:"ID"`
	Nombre    string   `json:"nombre"`
	Expresion string   `json:"expresion"`
	Etiquetas []string `json:"etiquetas"`
	Estatus   string   `json:"estatus"`
	FechaHora string   `json:"fechahora"`
}

//#########################< FUNCIONES GENERALES MGO >###############################

//GetAll Regresa todos los documentos existentes de Mongo (Por Coleccion)
func GetAll() []ExpresionRegularMgo {
	var result []ExpresionRegularMgo
	ExpresionRegulars, err := MoConexion.GetColectionMgo(MoVar.ColeccionExpresionRegular)
	if err != nil {
		fmt.Println(err)
	}
	err = ExpresionRegulars.Find(nil).All(&result)
	if err != nil {
		fmt.Println(err)
	}

	return result
}

//CountAll Regresa todos los documentos existentes de Mongo (Por Coleccion)
func CountAll() int {
	var result int
	ExpresionRegulars, err := MoConexion.GetColectionMgo(MoVar.ColeccionExpresionRegular)

	if err != nil {
		fmt.Println(err)
	}
	result, err = ExpresionRegulars.Find(nil).Count()
	if err != nil {
		fmt.Println(err)
	}

	return result
}

//GetOne Regresa un documento específico de Mongo (Por Coleccion)
func GetOne(ID bson.ObjectId) ExpresionRegularMgo {
	var result ExpresionRegularMgo
	ExpresionRegulars, err := MoConexion.GetColectionMgo(MoVar.ColeccionExpresionRegular)
	if err != nil {
		fmt.Println(err)
	}
	err = ExpresionRegulars.Find(bson.M{"_id": ID}).One(&result)
	if err != nil {
		fmt.Println(err)
	}

	return result
}

//GetEspecifics rsegresa un conjunto de documentos específicos de Mongo (Por Coleccion)
func GetEspecifics(Ides []bson.ObjectId) []ExpresionRegularMgo {
	var result []ExpresionRegularMgo
	var aux ExpresionRegularMgo
	ExpresionRegulars, err := MoConexion.GetColectionMgo(MoVar.ColeccionExpresionRegular)
	if err != nil {
		fmt.Println(err)
	}
	for _, value := range Ides {
		aux = ExpresionRegularMgo{}
		ExpresionRegulars.Find(bson.M{"_id": value}).One(&aux)
		result = append(result, aux)
	}

	return result
}

//GetEspecificByFields regresa un documento de Mongo especificando un campo y un determinado valor
func GetEspecificByFields(field string, valor interface{}) ExpresionRegularMgo {
	var result ExpresionRegularMgo
	ExpresionRegulars, err := MoConexion.GetColectionMgo(MoVar.ColeccionExpresionRegular)

	if err != nil {
		fmt.Println(err)
	}
	err = ExpresionRegulars.Find(bson.M{field: valor}).One(&result)
	if err != nil {
		fmt.Println(err)
	}

	return result
}

//GetIDByField regresa un documento específico de Mongo (Por Coleccion)
func GetIDByField(field string, valor interface{}) bson.ObjectId {
	var result ExpresionRegularMgo
	ExpresionRegulars, err := MoConexion.GetColectionMgo(MoVar.ColeccionExpresionRegular)
	if err != nil {
		fmt.Println(err)
	}
	err = ExpresionRegulars.Find(bson.M{field: valor}).One(&result)
	if err != nil {
		fmt.Println(err)
	}
	return result.ID
}

//CargaComboExpresionRegulars regresa un combo de ExpresionRegular de mongo
func CargaComboExpresionRegulars(ID string) string {
	ExpresionRegulars := GetAll()

	templ := ``

	if ID != "" {
		templ = `<option value="">--SELECCIONE--</option> `
	} else {
		templ = `<option value="" selected>--SELECCIONE--</option> `
	}

	for _, v := range ExpresionRegulars {
		if ID == v.ID.Hex() {
			templ += `<option value="` + v.ID.Hex() + `" selected>  ` + v.Nombre + ` </option> `
		} else {
			templ += `<option value="` + v.ID.Hex() + `">  ` + v.Nombre + ` </option> `
		}

	}
	return templ
}

//MapaModelo regresa el mapa del modelo para jqgrid
func MapaModelo() map[string]interface{} {
	var ModelOptions map[string]interface{}
	ModelOptions = make(map[string]interface{})
	ModelOptions["name"] = []string{"ID", "Nombre", "Expresion", "Etiquetas", "Estatus", "FechaHora"}
	ModelOptions["index"] = []string{"ID", "Nombre", "Expresion", "Etiquetas", "Estatus", "FechaHora"}
	ModelOptions["align"] = []string{"left", "left", "left", "center", "left", "left"}
	ModelOptions["editable"] = []bool{false, false, false, false, false, false}
	ModelOptions["hidden"] = []bool{false, false, false, false, false, false}
	ModelOptions["search"] = []bool{true, true, true, true, true, true}
	ModelOptions["stype"] = []string{"text", "text", "text", "text", "select", "text"}
	ModelOptions["sorteable"] = []bool{true, true, true, true, true, true}
	ModelOptions["sorttype"] = []string{"text", "text", "text", "text", "text", "date"}

	var Textos map[string]interface{}
	Textos = make(map[string]interface{})
	Textos["sopt"] = []string{"eq", "ne", "cn"}

	var Numeros map[string]interface{}
	Numeros = make(map[string]interface{})
	Numeros["sopt"] = []string{"eq", "ne", "lt", "le", "gt", "ge"}

	var Fechas map[string]interface{}
	Fechas = make(map[string]interface{})
	Fechas["sopt"] = []string{"eq", "ne", "lt", "le", "gt", "ge"}
	Fechas["dataInit"] = `datePick`

	var Formatos map[string]interface{}
	Formatos = make(map[string]interface{})
	Formatos["srcformat"] = "d m y"
	Formatos["newformat"] = "d m y"

	Fechas["formatoptions"] = Formatos

	var Atributos map[string]interface{}
	Atributos = make(map[string]interface{})
	Atributos["title"] = "Seleccionar Fecha"

	Fechas["attr"] = Atributos

	ModelOptions["searchoptions"] = []interface{}{Textos, Textos, Textos, Textos, Textos, Fechas}

	return ModelOptions
}

//GeneraRenglonesIndex crea un mapa para crear el json de renglones del index
func GeneraRenglonesIndex(Regexps []ExpresionRegularMgo) map[string]interface{} {
	var Mapas []map[string]interface{}

	for _, v := range Regexps {
		var Mapa map[string]interface{}
		Mapa = make(map[string]interface{})
		Mapa["ID"] = v.ID.Hex()
		Mapa["Nombre"] = v.Nombre
		Mapa["Expresion"] = v.Expresion
		var etiquetas string
		for _, eti := range v.Etiquetas {
			etiquetas += eti + "|"
		}
		etiquetas = etiquetas[:len(etiquetas)-1]
		Mapa["Etiquetas"] = etiquetas
		Mapa["Estatus"] = v.Estatus.Hex()
		Mapa["FechaHora"] = v.FechaHora.Format("2006/1/2")
		Mapas = append(Mapas, Mapa)

	}
	var Mapita map[string]interface{}
	Mapita = make(map[string]interface{})

	Mapita["rows"] = Mapas
	return Mapita
}

//GeneraTemplatesBusqueda crea templates de tabla de búsqueda
func GeneraTemplatesBusqueda(ExpresionRegulars []ExpresionRegularMgo) (string, string) {
	//floats := accounting.Accounting{Symbol: "", Precision: 2}
	cuerpo := ``

	cabecera := `<tr>
			<th>#</th>

				<th>Id</th>

				<th>Nombre</th>

				<th>Expresion</th>

				<th>Etiquetas</th>

				<th>Estatus</th>

				<th>FechaHora</th>
				</tr>`

	for k, v := range ExpresionRegulars {
		cuerpo += `<tr id = "` + v.ID.Hex() + `" onclick="window.location.href = '/ExpresionRegulars/detalle/` + v.ID.Hex() + `';">`
		cuerpo += `<td>` + strconv.Itoa(k+1) + `</td>`
		cuerpo += `<td>` + v.ID.Hex() + `</td>`

		cuerpo += `<td>` + v.Nombre + `</td>`

		cuerpo += `<td>` + v.Expresion + `</td>`

		for _, el := range v.Etiquetas {
			cuerpo += `<td>` + el + `</td>`
		}
		//		cuerpo += `<td>` + v.Etiquetas + `</td>`

		cuerpo += `<td>` + v.Estatus.Hex() + `</td>`
		//cuerpo += `<td>` + v.Estatus + `</td>`

		cuerpo += `<td>` + v.FechaHora.Format("2006-01-02") + `</td>`

		cuerpo += `</tr>`
	}

	return cabecera, cuerpo
}

//########################< FUNCIONES GENERALES PSQL >#############################

//######################< FUNCIONES GENERALES ELASTIC >############################

//BuscarEnElastic busca el texto solicitado en los campos solicitados
func BuscarEnElastic(texto string) (*elastic.SearchResult, error) {
	textoTilde, textoQuotes := MoGeneral.ConstruirCadenas(texto)

	queryTilde := elastic.NewQueryStringQuery(textoTilde)
	queryQuotes := elastic.NewQueryStringQuery(textoQuotes)

	queryTilde = queryTilde.Field("Nombre")
	queryQuotes = queryQuotes.Field("Nombre")

	queryTilde = queryTilde.Field("Expresion")
	queryQuotes = queryQuotes.Field("Expresion")

	queryTilde = queryTilde.Field("Etiquetas")
	queryQuotes = queryQuotes.Field("Etiquetas")

	queryTilde = queryTilde.Field("Estatus")
	queryQuotes = queryQuotes.Field("Estatus")

	var docs *elastic.SearchResult
	var err error

	docs, err = MoConexion.BuscaElastic(MoVar.TipoExpresionRegular, queryTilde)
	if err != nil {
		fmt.Println(err)
		fmt.Println("Falló el primer intento con: ", textoTilde)

		docs, err = MoConexion.BuscaElastic(MoVar.TipoExpresionRegular, queryQuotes)
		if err != nil {
			fmt.Println(err)
			fmt.Println("Falló el segundo intento con: ", textoQuotes)
			return nil, err
		}
		if docs.Hits.TotalHits == 0 {
			fmt.Println("No se encontraron resultados en el segundo intento con: ", textoQuotes)
		}
		return docs, nil

	}

	if docs.Hits.TotalHits == 0 {
		fmt.Println("No se encontraron resultados en el primer intento con: ", textoTilde)

		docs, err = MoConexion.BuscaElastic(MoVar.TipoExpresionRegular, queryQuotes)
		if err != nil {
			fmt.Println(err)
			fmt.Println("Falló el segundo intento con: ", textoQuotes)
			return nil, err
		}

		if docs.Hits.TotalHits == 0 {
			fmt.Println("No se encontraron resultados en el segundo intento con: ", textoQuotes)
		}

		return docs, nil
	}

	return docs, nil
}
