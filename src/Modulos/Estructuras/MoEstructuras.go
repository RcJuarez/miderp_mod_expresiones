package MoEstructuras

//TiposDeColumna estrcutura de opciones de columna para jqgrid
type TiposDeColumna struct {
	name          string
	index         string
	align         string
	datefmt       string
	editable      bool
	hidden        bool
	search        bool
	searchoptions map[string]interface{}
	soorteable    bool
	sorttype      string
	stype         string
	surl          string
	title         bool
	width         int
}

//PeticionJqGrid estructura que guarda todos los datos de la peticion que hace jqgrid
type PeticionJqGrid struct {
	Numpags int
	Page    int
	Sidx    string
	Sord    string
	Search  bool
	Rows    int
	Filtros map[string]interface{}
}
