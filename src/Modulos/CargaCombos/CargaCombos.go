package CargaCombos

import (
	"strconv"

	"gopkg.in/mgo.v2/bson"
)

//CargaComboSexo carga el combo de sexos
func CargaComboSexo(SexoSelect string) string {
	var Sexos = []string{"Masculino", "Femenino"}
	templ := ``
	for _, Sexo := range Sexos {
		if Sexo == SexoSelect {
			templ += `<option value="` + Sexo + `" selected>` + Sexo + `</option>`
		} else {
			templ += `<option value="` + Sexo + `">` + Sexo + `</option>`
		}
	}
	return templ
}

//CargaComboMostrarEnIndex carga las opciones de mostrar en el index
func CargaComboMostrarEnIndex(Muestra int) string {
	var Cantidades = []int{5, 10, 15, 20}
	templ := ``

	for _, v := range Cantidades {
		if Muestra == v {
			templ += `<option value="` + strconv.Itoa(v) + `" selected>` + strconv.Itoa(v) + `</option>`
		} else {
			templ += `<option value="` + strconv.Itoa(v) + `">` + strconv.Itoa(v) + `</option>`
		}
	}
	return templ
}

//ArrayStringToObjectID Convierte un Arreglo de String a uno de Objects Ids
func ArrayStringToObjectID(ArrayStr []string) []bson.ObjectId {
	var ArrayID []bson.ObjectId
	for _, d := range ArrayStr {
		if bson.IsObjectIdHex(d) {
			ArrayID = append(ArrayID, bson.ObjectIdHex(d))
		}
	}
	return ArrayID
}

//CargaComboConexiones carga un listado de conexiones acia postgres
/*func CargaComboConexiones(idConexion string) string {
	conexiones := ConexionModel.GetAll()
	templ := ``

	if idConexion != "" {
		templ = `<option value="">--SELECCIONE--</option>`
	} else {
		templ = `<option value="" selected>--SELECCIONE--</option>`
	}

	for _, val := range conexiones {
		if idConexion == val.ID.Hex() {
			templ += `<option value="` + val.ID.Hex() + `" selected>` + val.Nombre + `[` + val.Servidor + `]` + `</option>`
		} else {
			templ += `<option value="` + val.ID.Hex() + `">` + val.Nombre + `[` + val.Servidor + `]` + `</option>`
		}
	}

	return templ
}*/
