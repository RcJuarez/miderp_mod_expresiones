package ExprRedis

import (
	"encoding/json"
	"errors"
	"regexp"
	"strings"

	"../../Modelos/ExpresionRegularModel"
	"../../Modulos/Conexiones"
	"../../Modulos/Redis"
	elastic "gopkg.in/olivere/elastic.v5"
)

//RegularExpresion es una estructura
/*type RegularExpresion struct {
	ID        string   `json:"ID"`
	Nombre    string   `json:"nombre"`
	Expresion string   `json:"expresion"`
	Etiquetas []string `json:"etiquetas"`
	Estatus   string   `json:"estatus"`
	FechaHora string   `json:"fechahora"`
}*/

//BuscaExpresiones busca las expresiones
func BuscaExpresiones(termino string) ([]ExpresionRegularModel.ExpresionRegularRedis, error) {
	nombreHashes, err := Redis.ObtenerMiembrosdeGrupo("expresiones")
	if err != nil {
		return nil, err
	}

	var todo []map[string]string

	for _, item := range nombreHashes {
		aux, err := Redis.RegresaValoresHash(item)
		if err != nil {
			return nil, err
		}
		todo = append(todo, aux)
	}

	var match []map[string]string
	for _, item := range todo {
		for _, value := range item {
			if strings.Contains(value, termino) {
				match = append(match, item)
				break
			}
		}
	}

	var datos []ExpresionRegularModel.ExpresionRegularRedis
	for _, item := range match {
		y := strings.Split(item["Tags"], "|")
		auxString := "["
		for _, jit := range y {
			auxString += `"` + jit + `",`
		}
		auxString = auxString[:(len(auxString) - 1)]
		auxString += "]"

		var auxRegExp ExpresionRegularModel.ExpresionRegularRedis
		byt := []byte(`{"ID":"` + item["ID"] + `","Cadena":"` + item["Cadena"] + `","Descripcion":"` + item["Descripcion"] + `","Tags":` + auxString + `,"Status":"` + item["Status"] + `"}`)
		err := json.Unmarshal(byt, &auxRegExp)
		if err != nil {
			return nil, err
		}
		datos = append(datos, auxRegExp)
	}

	return datos, nil
}

//BuscaPorID busca por ID personalizado
func BuscaPorID(ID string) (string, error) {
	query := elastic.NewQueryStringQuery(ID)
	query = query.Field("ID")

	docs, err := MoConexion.BusquedaElastic("regexp", query)
	if err != nil {
		return "", err
	}
	var datos []string

	if len(docs.Hits.Hits) <= 0 {
		return "", errors.New("No hay datos")
	}

	for _, item := range docs.Hits.Hits {
		aux := item.Id
		datos = append(datos, aux)
	}
	return datos[0], nil
}

//AltaExpresion da de alta una expresion en Redis
func AltaExpresion(id string, nombre string, expresion string, etiquetas []string, fechahora string) bool {

	//Inserta en Redis
	var eti string
	for _, item := range etiquetas {
		eti += item + "|"
	}
	//eti = eti[:len(eti)-1]

	byt := []byte(`{"ID":"` + id + `","Nombre":"` + nombre + `","Expresion":"` + expresion + `","Etiquetas":"` + eti + `","Estatus":"activo","FechaHora":"` + fechahora + `"}`)
	var datos map[string]interface{}
	err := json.Unmarshal(byt, &datos)
	if err != nil {
		return false
	}

	_, err = Redis.InsertarAConjunto("expresiones", id)
	if err != nil {
		return false
	}

	err = Redis.CrearHash(id, datos)
	if err != nil {
		return false
	}

	return true
}

//ActualizaExpresion actualiza los datos de la expresion
func ActualizaExpresion(dato ExpresionRegularModel.ExpresionRegularRedis) bool {
	var etiquetas string
	for _, item := range dato.Etiquetas {
		etiquetas += item + "|"
	}

	byt := []byte(`{"ID":"` + dato.ID + `","Nombre":"` + dato.Nombre + `","Expresion":"` + dato.Expresion + `","Etiquetas":"` + etiquetas + `","Estatus":"activo", "FechaHora":"` + dato.FechaHora + `"}`)
	var datos map[string]interface{}
	err := json.Unmarshal(byt, &datos)
	if err != nil {
		return false
	}
	err = Redis.ActualizarHash(dato.ID, datos)
	if err != nil {
		return false
	}

	return true
}

//EliminaExpresion elimina la expresion de Redis y Elasticsearch
func EliminaExpresion(id string) bool {
	err := Redis.EliminaMiembroDeGrupo("expresiones", id)
	if err != nil {
		return false
	}

	/*err = Redis.EliminarHash(id)

	if err != nil {
		return false
	}*/
	valor, err := BuscaPorID(id)
	if err != nil {
		return false
	}
	return MoConexion.DeleteElastic("regexp", valor)
}

//GetExpresionByID retorna un string con la expresion, y un error si es que algo ha salido mal
func GetExpresionByID(id string) (string, error) {
	return Redis.RegresaDatoHash(id, "Cadena")
}

//ValidaExpresion valida una cadena con una expresión dada; retorna un booleano y un string
func ValidaExpresion(id string, cadenaAValidar string) (bool, string) {
	cadena, err := GetExpresionByID(id)
	if err != nil {
		return false, ""
	}
	valida, err := regexp.MatchString(cadena, cadenaAValidar)
	if err != nil {
		return false, ""
	}
	if valida {
		return true, ""
	}
	return false, cadena
}
