package MoConexion

import (
	"bytes"
	"encoding/base64"
	"fmt"
	"image/jpeg"
	"image/png"
	"io"
	"log"
	"mime/multipart"
	"os"
	"path/filepath"

	"../../Modulos/Variables"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

//DataM es una estructura que contiene los datos de configuración en el archivo cfg
var DataM = MoVar.CargaSeccionCFG(MoVar.SecMongo)

//SesionMongo contiene una sesion de mongo para ser utilizado en cualquier parte
var SesionMongo *mgo.Session

//check verifica y escribe el error que elige el usuario y grita el error general
func check(err error, mensaje string) {
	if err != nil {
		fmt.Println("##########")
		fmt.Println(mensaje)
		fmt.Println("##########")
		panic(err)
	}
}

//GetColectionMgo Obtiene el contenido de una coleccion y un error en caso de que ocurra
//Entrada: coleccion : nombre de la coleccion que se desee obtener
//Salida:*mgo.Collection: Coleccion de datos especificados
//Salida: error: error en caso de que fallara al obtener la colección
//Autor: Ismael Hernández
//FechaCreacion: ---
//MOdificacion: 07/09/2017 --- Se elimina el abrir la conexión, y se consulta una conexion ya existente
func GetColectionMgo(coleccion string) (*mgo.Collection, error) {
	/*
		s, err := mgo.Dial(DataM.Servidor)
		if err != nil {
			return nil, nil, err
		}
	*/
	s, err := ObtenerSesionAbiertaMgo()
	//defer s.Close()
	if err != nil {
		return nil, err
	}

	c := s.DB(DataM.NombreBase).C(coleccion)
	return c, err
}

//CerrarSesionAbierta Cierra la sesion que se tiene abierta
//---Entrada:---:---
//---Salida:---:---
//---Autor: Ramón Cruz Juárez
//---FechaCreacion: 07/09/2017
//---Modificacion:---:---
func CerrarSesionAbierta() {
	informacion, err := SesionMongo.BuildInfo()
	if err != nil {
		fmt.Println("Hay errores al buscar informacion: ", err)
	}
	fmt.Println("Cerrando las sesiones de MongoDb", informacion)
	SesionMongo.Close()
	SesionMongo = nil
}

//GetColectionMgoSesion Obtiene el contenido de una coleccion y un error en caso de que ocurra
//Entrada:coleccion: nombre de la coleccion que se desee obtener
//Entrada:sesion: sesion del servidor al que se conecta
//Salida:*mgo.Collection: Coleccion de datos especificados
//Salida: error: error en caso de que fallara al obtener la colección
//Autor: Ramón Cruz Juárez
//FechaCreacion: 29/08/2017
//MOdificacion: fecha --- Descripcion
func GetColectionMgoSesion(coleccion string, sesion *mgo.Session) (*mgo.Collection, error) {
	c := sesion.DB(DataM.NombreBase).C(coleccion)
	return c, nil
}

/*
//GetConexionMgo regresa una sesion de mgo y error
func GetConexionMgo() (*mgo.Session, error) {
	session, err := mgo.Dial(DataM.Servidor)
	if err != nil {
		return session, err
	}
	return session, nil
}
*/

//ObtenerSesionAbiertaMgo Obtiene una sesion de mongodb que previamente ah sido abierta
//Entrada: variable : descripcion
//Salida: *mgo.Session : Sesion previamente abierta o nulo en caso de que no se ha abierto
//Autor: Ramón Cruz Juárez
//FechaCreacion: 07/09/2017
//MOdificacion: 10/09/2017 --- Desde éste metodo se abre la sesion en caso de que no exista uno abierto
func ObtenerSesionAbiertaMgo() (*mgo.Session, error) {
	/*
		err := SesionMongo.Ping()
		return SesionMongo, err
	*/
	/*
		const (
			Host     = "localhost:27017"
			Username = ""
			Password = ""
		)

		SesionMongo, err := mgo.DialWithInfo(&mgo.DialInfo{
			Addrs:    []string{Host},
			Username: Username,
			Password: Password,
			DialServer: func(addr *mgo.ServerAddr) (net.Conn, error) {
				return tls.Dial("tcp", addr.String(), &tls.Config{})
			},
		})
		if err != nil {
			panic(err)
		}
		defer SesionMongo.Close()

		fmt.Printf("Connected to %v!\n", SesionMongo.LiveServers())
		//defer SesionMongo.Close()
	*/

	var err error
	if SesionMongo == nil {
		fmt.Println("la conexin es nula: ", SesionMongo)
		SesionMongo, err = mgo.Dial(DataM.Servidor)
		if err != nil {
			fmt.Println("Hay errores al buscar informacion: ", err)
		}
		fmt.Println("Levantando la  sesion de mongo: ", SesionMongo)
	}
	fmt.Printf("Connected to %v!\n", SesionMongo.LiveServers())

	return SesionMongo, err
}

//SetSesionMgo Establece una sesion a la base de datos MongoDb
//Entrada: variable : descripcion
//Salida:variable: descripcion
//Autor: Ramón Cruz Juárez
//FechaCreacion: 29/08/2017
//MOdificacion: 10/09/2017 --- Ésta Función se deja de utilizar, debido a que la funcionalidad es reemplazadada por el metodo obtenersesionabiertamgo()
/*
func SetSesionMgo() {
	var err error
	SesionMongo, err = mgo.Dial(DataM.Servidor)
	if err != nil {
		fmt.Println("Errores al conectar mongo: ", err)
	} else {
		informacion, err := SesionMongo.BuildInfo()
		if err != nil {
			fmt.Println("Hay errores al buscar informacion: ", err)
		}
		fmt.Println("Imprimiendo la informacion de la sesion", SesionMongo)
		fmt.Println("OpenSSLVersion", informacion.OpenSSLVersion)
		fmt.Println("SysInfo", informacion.SysInfo)
		fmt.Println("GitVersion", informacion.GitVersion)
	}
}
*/

//CloseConexionMgo cierra la sesion que se especifica
func CloseConexionMgo(sesion *mgo.Session) {
	sesion.Close()
}

//GetBaseMgo regresa un objeto database de mgo específico de una sesion específica
func GetBaseMgo(base string, sesion *mgo.Session) *mgo.Database {
	sesion.SetMode(mgo.Monotonic, true)
	return sesion.DB(base)
}

//InsertarImagen inserta una imagen en mongo y en el directorio ./Recursos/Imagenes
func InsertarImagen(file multipart.File, header *multipart.FileHeader) (string, error) {
	defer file.Close()

	nombrefile := header.Filename
	dirpath := "./Recursos/Imagenes"

	//Comprobar directorio y crearlo
	if _, err := os.Stat(dirpath); os.IsNotExist(err) {
		fmt.Println("el directorio no	 existe")
		os.MkdirAll(dirpath, 0777)
	}

	//subir imagen al servidor local
	out, err := os.Create("./Recursos/Imagenes/" + nombrefile)
	if err != nil {
		return "No es posible crear el archivo en el directorio, compruebe los permisos", err
	}
	defer out.Close()

	_, err = io.Copy(out, file)
	if err != nil {
		return "Error al escribir la imagen al directorio", err
	}

	//Inserar la imagen en Mongo
	idsImg, err := UploadImageToMongodb(dirpath, nombrefile)

	return idsImg, err
}

//UploadImageToMongodb Inserta Imagen en Mongo y devuelve su Id
func UploadImageToMongodb(path string, namefile string) (string, error) {
	/*
		db, err := mgo.Dial(DataM.Servidor)
		if err != nil {
			check(err, "Error al conectar con mongo")
			return "Error al conectar con mongo", err
		}
	*/
	s, err := ObtenerSesionAbiertaMgo()
	//defer s.Close()
	if err != nil {
		return "Error al conectar con mongo", err
	}
	base := s.DB(DataM.NombreBase)

	file2, err := os.Open(path + "/" + namefile)
	if err != nil {
		check(err, "Error al abrir el archivo o el archivo no existe")
		return "Error al abrir el archivo o el archivo no existe", err
	}
	defer file2.Close()

	stat, err := file2.Stat()
	if err != nil {
		check(err, "Error al leer el archivo")
		return "Error al leer el archivo", err
	}

	bs := make([]byte, stat.Size()) // read the file
	_, err = file2.Read(bs)
	if err != nil {
		check(err, "Error al crear objeto que contendrá el archivo")
		return "Error al crear objeto que contendrá el archivo", err
	}

	img, err := base.GridFS("Imagenes").Create(namefile)
	if err != nil {
		check(err, "error al crear archivo en mongo")
		return "Error al crear archivo en mongo", err
	}

	idsImg := img.Id()
	_, err = img.Write(bs)
	if err != nil {
		check(err, "error al escribir archivo en mongo")
		return "Error al escribir archivo en mongo", err
	}

	fmt.Println("File uploaded successfully to mongo ")
	err = img.Close()
	if err != nil {
		check(err, "error al cerrar img de mongo")
		return "Error al cerrar img de mongo", err
	}

	idimg := getObjectIDToInterface(idsImg)

	return idimg.Hex(), nil
}

func getObjectIDToInterface(i interface{}) bson.ObjectId {
	var v = i.(bson.ObjectId)
	return v
}

//RegresaTagImagen regresa el tag de la imagen con ID correspondiente
func RegresaTagImagen(ID string) (string, error) {
	objid := bson.ObjectIdHex(ID)
	/*
		db, err := mgo.Dial(DataM.Servidor)
		if err != nil {
			check(err, "Error al conectar con mongo")
			return "Error al conectar con mongo", err
		}
	*/
	s, err := ObtenerSesionAbiertaMgo()
	//defer s.Close()
	if err != nil {
		return "Error al conectar con mongo", err
	}
	base := s.DB(DataM.NombreBase)

	img, err := base.GridFS("Imagenes").OpenId(objid)
	if err != nil {
		check(err, "Error al obtener imagen")
		return "Error al obtener imagen", err
	}
	b := make([]byte, img.Size())
	_, errim := img.Read(b)
	if errim != nil {
		fmt.Println("Error al leer la imagen...", err)
	}

	var tmp = ``
	///////////////////////////////////////////////////////////////////////////
	switch extension := filepath.Ext(img.Name()); extension {
	case ".jpg", ".jpeg":
		imagen, _ := jpeg.Decode(bytes.NewReader(b))
		buffer := new(bytes.Buffer)
		if err := jpeg.Encode(buffer, imagen, nil); err != nil {
			log.Println("unable to encode image.")
			return "No es posible decodificar imagen", err
		}
		str := base64.StdEncoding.EncodeToString(buffer.Bytes())
		tmp = `data:image/jpg;base64,` + str

	case ".png":

		imagen, _ := png.Decode(bytes.NewReader(b))
		buffer := new(bytes.Buffer)
		if err := png.Encode(buffer, imagen); err != nil {
			log.Println("unable to encode image.")
			return "No es posible decodificar imagen", err
		}
		str := base64.StdEncoding.EncodeToString(buffer.Bytes())
		tmp = `data:image/png;base64,` + str

	}
	return tmp, nil
}
