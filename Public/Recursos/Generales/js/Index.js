var FiltrosDeGrid;

function GeneraGrid(EncabezadoDeGrid, CadenaURLDeMetodoParaObtenerDatos, ListadoDeEncabezadosDeGrid, ListadoDeColumnasDeModeloDeGrid, MetodoSelectRow, Surname, IdList, IdPager, MetodoAlFinalizarCarga, loadOnce, MetodoAfterInsertRow) {
    var FiltrosComplejos = "";
    var FiltrosToolbar = "";
    var Filtros = "";
    if (!IdList) {
        IdList = '#list'
    }

    if (!IdPager) {
        IdPager = '#pager'
    }
    if (!loadOnce) {
        loadOnce = false;
    }

    function resize_the_grid() {
        $(IdList).setGridWidth($("#SeccionCuerpo").width() * .95);
    }

    //Funcion que recibe una cadena y regresa un combo con los elementos de la misma
    formatSelect = function(cellValue, options, rowObject) {
        console.log(cellValue)
        var select = "<select>"; 
        var items = cellValue.split("|");
        if(items[0] == ""){
            select += "<option>Sin elementos</option>"
        }else{
            for (i in items) {
                select += "<option>"+items[i]+"</option>";
            }
        }
        select += "</select>"
        return select;
    }

    datePick = function(elem) {
        jQuery(elem).datepicker({
            dateFormat: 'yy/m/d',
            maxDate: new Date(2020, 0, 1),
            showOn: 'focus',
            onClose: function(dateText, inst) {
                $(elem).focus().trigger({ type: 'keypress', charCode: 13 });
                $(elem).focus();
            }
        });
    }
    $.jgrid.gridUnload(IdList)

    resize_the_grid();

    $(document).ready(function() {
        $(IdList).jqGrid({
            datatype: "local",
            data: CadenaURLDeMetodoParaObtenerDatos,

            // mtype: "GET",
            // datatype: "json",
            // url: CadenaURLDeMetodoParaObtenerDatos,

            styleUI: 'Bootstrap',
            colNames: ListadoDeEncabezadosDeGrid,
            colModel: ListadoDeColumnasDeModeloDeGrid(),
            pager: IdPager,
            rowNum: 10,
            rowList: [5, 10, 20, 50, 100],
            sortname: Surname,
            sortorder: 'desc',
            hidegrid: false,
            viewrecords: true,
            caption: EncabezadoDeGrid,
            scroll: false,
            loadonce: loadOnce, //dependiendo de este parámetro se harán peticiones a la base por cada acción de ordenamiento, busqueda, etc. o no.
            multiselect: false, //estos dos permiten seleccionar variso registros a la vez, en automático añade a la izquierda un control de tipo check box (los dos deben estar en true)
            multiboxonly: false,
            toolbar: [true, "top"], //establece una barra de búsqueda, este parámetro va ligado con las opciones de filter Toolbar mas abajo
            treegrid: true,
            colMenu: false,
            coloptions: { sorting: true, columns: true, filtering: false, seraching: false, grouping: true, freeze: false },
            shrinkToFit: true,
            altRows: true,
            onSelectRow: MetodoSelectRow,
            loadComplete: MetodoAlFinalizarCarga,
            afterInsertRow: MetodoAfterInsertRow

        });

        $(IdList).jqGrid('filterToolbar', {
            stringResult: true,
            searchOperators: true,
            searchOnEnter: true,
            enableClear: true
        });

        $(IdList).jqGrid('navGrid', IdPager, {
            search: true,
            add: false,
            edit: false,
            del: false,
            refresh: true,
            view: false,
            position: "left",
            cloneToTop: true
        }, {}, {}, {}, {
            multipleSearch: true,
            afterRedraw: function($p) {
                $("select.opsel").remove();
            },
            closeOnEscape: true,
            closeAfterSearch: true
        });


        $(IdList).navButtonAdd(IdPager, {
            buttonicon: "ui-icon-calculator",
            title: "Seleccionar Columnas",
            caption: "Ordenar y Seleccionar Columnas",
            position: "last",
            onClickButton: function() {
                $(IdList).jqGrid('columnChooser', {
                    done: function(perm) {
                        if (perm) {
                            this.jqGrid("remapColumns", perm, true);
                            resize_the_grid();
                        }
                    }
                });
            }
        });
        resize_the_grid();
    });

}