

//##############################< SCRIPTS JS >##########################################
//################################< ExpresionRegular.js >#####################################
//#########################< VALIDACIONES DE JEQUERY >##################################

$(document).ready(function (){
	
	if(document.getElementById("tbody_etiquetas_etiquetas") !==  null){
		if (document.getElementById("tbody_etiquetas_etiquetas").children.length == 0){
			$('#div_tabla_etiquetas').hide();
		}
	}

	$('#AgregaEtiqueta').click(function () {
	
		var validator = valida();

		if ($('#Etiqueta').val() != "") {
			
			$('#div_tabla_etiquetas').show();
			$("#tbody_etiquetas_etiquetas").append(
				'<tr>\n\
			<td><input type="hidden" class="form-control" value=""><input type="text" class="form-control etiquetas" name="Etiquetas" value="' + $("#Etiqueta").val() + '" readonly></td>\n\
			<td><button type="button" class="btn btn-primary editButton"><span class="glyphicon glyphicon-pencil btn-xs"></span></button><button type="button" class="btn btn-danger deleteButton"><span class="glyphicon glyphicon-trash btn-xs"></span></button></td>\n\
			</tr>');

			$("#Etiqueta").val("");
			$("#Etiqueta").focus();
		} else {	
			validator.showErrors({
				"Etiqueta": "No puede agregar Etiquetas vacías"
			});
			$("#Etiqueta").focus();
		}

	});	

	$('#Etiqueta').keydown(function(e) {
		if(e.which == 13 || e.keyCode == 13) {
				e.preventDefault();
				$('#AgregaEtiqueta').trigger("click");
		}
	});	

	$('.etiquetas').keydown(function(e) {
		if(e.which == 13 || e.keyCode == 13) {
			e.preventDefault();
		}	
	});	

	$(document).on('click', '.deleteButton', function () {
		$(this).parent().parent().remove();
		if (document.getElementById("tbody_Expresionregular").children.length == 0){
			$('#div_tabla_ExpresionesRegulares').hide();
		}
	});
	$(document).on('click', '.editButton', function () {
		$(this).parent().parent().children().children()[1].readOnly = false;
		$(this).parent().parent().children().children()[1].focus();
	});
		
});

function valida() {
	var validator = $("#Form_Alta_ExpresionRegular").validate({
		rules: {

			Nombre: {

				required: true,

				rangelength: [10, 50]

			},
			Expresion: {

				required: true,

				rangelength: [1, 255]
			},


		},

		messages: {

			Nombre: {
				
				required: "El campo Nombre es requerido.",
				rangelength: "La longitud del campo Nombre debe estar entre  [10, 50]"
			},
			Expresion: {
				required: "El campo Expresion es requerido.",
				rangelength: "La longitud del campo Expresion debe estar entre  [10, 255]"
			},
		},
		errorElement: "em",
		errorPlacement: function (error, element) {
			error.addClass("help-block");
			element.parents(".col-sm-5").addClass("has-feedback");

			if (element.prop("type") === "checkbox") {
				error.insertAfter(element.parent("label"));
			} else {
				error.insertAfter(element);
			}

			if (!element.next("span")[0]) {
				$("<span class='glyphicon glyphicon-remove form-control-feedback'></span>").insertAfter(element);
			}
		},
		success: function (label, element) {
			if (!$(element).next("span")[0]) {
				$("<span class='glyphicon glyphicon-ok form-control-feedback'></span>").insertAfter($(element));
			}
		},
		highlight: function (element, errorClass, validClass) {
			$(element).parents(".col-sm-5").addClass("has-error").removeClass("has-success");
			$(element).next("span").addClass("glyphicon-remove").removeClass("glyphicon-ok");
		},
		unhighlight: function (element, errorClass, validClass) {
			$(element).parents(".col-sm-5").addClass("has-success").removeClass("has-error");
			$(element).next("span").addClass("glyphicon-ok").removeClass("glyphicon-remove");
		}
	});
	return validator;
}

function EditaExpresionRegular(vista) {

	if (vista == "Index" || vista == "") {
		alert($('#ExpresionRegulars').val());
		if ($('#ExpresionRegulars').val() != "") {
			window.location = '/ExpresionRegulars/edita/' + $('#ExpresionRegulars').val();
		} else {
			alertify.error("Debe Seleccionar un ExpresionRegular para editar");
		}
	} else if (vista == "Detalle") {
		if ($('#ID').val() != "") {
			window.location = '/ExpresionRegulars/edita/' + $('#ID').val();
		} else {
			alertify.error("No se puede editar debido a un error de referencias, favor de intentar en el index");
			window.location = '/ExpresionRegulars';
		}
	}

}

function DetalleExpresionRegular() {
	if ($('#ExpresionRegulars').val() != "") {
		window.location = '/ExpresionRegulars/detalle/' + $('#ExpresionRegulars').val();
	} else {
		alertify.error("Debe Seleccionar un ExpresionRegular para editar");
	}
}


function Evaluar() {
	id = document.getElementById('Expresion').value;
	evaluar = document.getElementById('Evaluar').value;
	$.ajax({
		url: "/ExpresionRegulars/Evaluar?ID=" + id + "&Evaluar=" + evaluar,
		type: 'GET',

		success: function (data) {
			console.log(data)
		},
		error: function (data) {
			console.log(data)

		},
	});
}

//Metodo para ocupar labels termina
document.getElementById("BotonEvaluar").addEventListener("click", function (e) {
	id = window.location.href.split("/")[window.location.href.split("/").length - 1];
	evaluar = document.getElementById('Evaluar').value;
	console.log(id, evaluar)
	$.ajax({
		url: "/ExpresionRegulars/Evaluar?ID=" + id + "&Evaluar=" + evaluar,
		type: 'GET',

		success: function (data) {
			console.log(data)
			if (data == 'true') {
				alertify.success("La expresion es correcta");
			} else {
				alertify.error("La expresion es incorrecta");
			}
		},
		error: function (data) {
			console.log(data)
		},
	});

});

/*
document.getElementById("BotonEvaluar").addEventListener("click", function (e) {
    alert("Evaluar func");
    id = window.location.href.split("/")[window.location.href.split("/").length - 1];
    evaluar = document.getElementById('Evaluar').value;
    console.log(id, evaluar)
    $.ajax({
        url: "/ExpresionRegulars/Evaluar?ID=" + id + "&Evaluar=" + evaluar,
        type: 'GET',

        success: function (data) {
            console.log(data)
            if (data == 'true') {
                alertify.success("La expresion es correcta");
            } else {
                alertify.error("La expresion es incorrecta");
            }
        },
        error: function (data) {
            console.log(data)
        },
    });

});*/
