package main

import (
	"fmt"

	"./src/Controladores/ExpresionRegularControler"
	"./src/Modulos/Variables"

	"gopkg.in/kataras/iris.v6"
	"gopkg.in/kataras/iris.v6/adaptors/httprouter"
	"gopkg.in/kataras/iris.v6/adaptors/sessions"
	"gopkg.in/kataras/iris.v6/adaptors/view"
)

func main() {
	//###################### Start ####################################
	app := iris.New()
	app.Adapt(httprouter.New())
	sesiones := sessions.New(sessions.Config{Cookie: "cookiekore"})
	app.Adapt(sesiones)

	app.Adapt(view.HTML("./Public/Vistas", ".html").Reload(true))

	app.Set(iris.OptionCharset("UTF-8"))

	app.StaticWeb("/icono", "./Public/Recursos/Generales/img")

	app.StaticWeb("/css", "./Public/Recursos/Generales/css")
	app.StaticWeb("/js", "./Public/Recursos/Generales/js")
	app.StaticWeb("/Plugins", "./Public/Recursos/Generales/Plugins")
	app.StaticWeb("/scripts", "./Public/Recursos/Generales/scripts")
	app.StaticWeb("/img", "./Public/Recursos/Generales/img")
	app.StaticWeb("/Comprobantes", "./Public/Recursos/Locales/comprobantes")
	app.StaticWeb("/Locales", "./Public/Recursos/Locales")

	//###################### CFG ######################################

	var DataCfg = MoVar.CargaSeccionCFG(MoVar.SecDefault)

	//###################### Ruteo ####################################

	//###################### HTTP ERRORS ################################
	/*
		//Error 403
		app.OnError(iris.StatusForbidden, HTTPErrors.Error403)
		//Error 404
		app.OnError(iris.StatusNotFound, HTTPErrors.Error404)

		//Error 500
		app.OnError(iris.StatusInternalServerError, HTTPErrors.Error500)*/

	// PRUEBAS DE ERRORES
	app.Get("/500", func(ctx *iris.Context) {
		ctx.EmitError(iris.StatusInternalServerError)
	})
	app.Get("/404", func(ctx *iris.Context) {
		ctx.EmitError(iris.StatusNotFound)
	})

	//###################### Login ################################
	//Login
	/*
		app.Get("/Login/saveMac", LoginControler.GetMac)

		// get
		app.Get("/", LoginControler.LoginGet)
		//Post
		app.Post("/", LoginControler.LoginPost)

		//Password Recovery
		//Get
		app.Get("/Recuperar", LoginControler.RecuperarGet)

		//Post
		app.Post("/Recuperar", LoginControler.RecuperarPost)

		//NuevoLogin
		//Get
		app.Get("/NuevoLogin", LoginControler.NuevoGet)

		app.Get("/Logout", LoginControler.Logout)
		//Post

		//Index
		app.Get("/Index", LoginControler.IndexGet)
	*/
	//###################### ExpresionRegular ################################
	//app.Get("/Index", IndexControler.IndexGet)
	//Index (Búsqueda)
	app.Get("/ExpresionRegulars", ExpresionRegularControler.IndexGet)
	app.Post("/ExpresionRegulars/CargaIndex", ExpresionRegularControler.CargaIndex)
	app.Post("/ExpresionRegulars/CargaIndexEspecifico", ExpresionRegularControler.CargaIndexEspecifico)

	//Alta
	app.Get("/ExpresionRegulars/alta", ExpresionRegularControler.AltaGet)
	app.Post("/ExpresionRegulars/alta", ExpresionRegularControler.AltaPost)

	//Edicion
	app.Get("/ExpresionRegulars/edita", ExpresionRegularControler.EditaGet)
	app.Post("/ExpresionRegulars/edita", ExpresionRegularControler.EditaPost)
	app.Get("/ExpresionRegulars/edita/:ID", ExpresionRegularControler.EditaGet)
	app.Post("/ExpresionRegulars/edita/:ID", ExpresionRegularControler.EditaPost)

	//Detalle
	app.Get("/ExpresionRegulars/detalle", ExpresionRegularControler.DetalleGet)
	app.Post("/ExpresionRegulars/detalle", ExpresionRegularControler.DetallePost)
	app.Get("/ExpresionRegulars/detalle/:ID", ExpresionRegularControler.DetalleGet)
	app.Post("/ExpresionRegulars/detalle/:ID", ExpresionRegularControler.DetallePost)

	//evaluar
	app.Get("/ExpresionRegulars/Evaluar", ExpresionRegularControler.EvaluarGet)
	//app.Post("/ExpresionRegulars/detalle/:Expresion", ExpresionRegularControler.EvaluarPost)

	//Rutinas adicionales

	//###################### Configuracion ################################
	//Configurar
	//Get
	//app.Get("/Configurar/", ConfiguracionControler.Configuracion)

	//###################### Otros #####################################

	//                        ...

	//###################### Listen Server #############################

	//###################### Problemas de Sistema #############################

	if DataCfg.Puerto != "" {
		fmt.Println("Ejecutandose en el puerto: ", DataCfg.Puerto)
		fmt.Println("Acceder a la siguiente url: ", DataCfg.BaseURL)
		app.Listen(":" + DataCfg.Puerto)
	} else {
		fmt.Println("Ejecutandose en el puerto: 8080")
		fmt.Println("Acceder a la siguiente url: localhost")
		app.Listen(":8080")
	}

}
